import React, { Component } from "react";

export default class SignUp extends Component {

    userData; 

    constructor(props) {
        super(props)

        this.state = {
            firstName: "",
            lastName: "",
            npiNumber: "",
            businessAddress: "",
            telephoneNumber: "",
            emailAddress: ""
        }
        this.handleSubmit=this.handleSubmit.bind(this)
    }

    firsthandler = (event) => {
        this.setState({
            firstName: event.target.value
        })
    }
    lasthandler = (event) => {
        this.setState({
            lastName: event.target.value
        })
    }

    npihandler = (event) => {
        this.setState({
            npiNumber: event.target.value
        })
    }

    bussinesshandler = (event) => {
        this.setState({
            businessAddress: event.target.value
        })
    }

    telephonehandler = (event) => {
        this.setState({
            telephoneNumber: event.target.value
        })
    }

    emailhandler = (event) => {
        this.setState({
           emailAddress: event.target.value
        })
    }

    handleSubmit = (event) => {
        alert(`${this.state.firstName} ${this.state.lastName}  ****  Registered Successfully ****`)
        console.log(this.state);
        this.setState({
            firstName: "",
            lastName: "",
            npiNumber: "",
            businessAddress: "",
            telephoneNumber: "",
            emailAddress: ""
            
        })
     event.preventDefault()
        
    }

  
  // React Life Cycle
    componentDidMount() {
        this.userData = JSON.parse(localStorage.getItem('user'));

        if (localStorage.getItem('user')) {
            this.setState({
                firstName: this.userData.firstName,
            lastName: this.userData.lastName,
            npiNumber: this.userData.npiNumber,
            businessAddress: this.userData.businessAddress,
            telephoneNumber: this.userData.telephoneNumber,
            emailAddress: this.userData.emailAddress
            })
        } else {
            this.setState({
                firstName: "",
            lastName: "",
            npiNumber: "",
            businessAddress: "",
            telephoneNumber: "",
            emailAddress: ""
            })
        }
    }

    componentWillUpdate(nextProps, nextState) {
        localStorage.setItem('user', JSON.stringify(nextState));
    }


    render() {
        return (
            <form  onSubmit={this.handleSubmit}>
                <h3>Healthcare Providers Registration</h3>

                <div className="form-group">
                    <label>First name</label>
                    <input type="text" value={this.state.firstName} onChange={this.firsthandler} className="form-control" placeholder="First name" />
                </div>

                <div className="form-group">
                    <label>Last name</label>
                    <input type="text"  value={this.state.lastName} onChange={this.lasthandler} className="form-control" placeholder="Last name" />
                </div>

                <div className="form-group">
                    <label>NPI number</label>
                    <input type="number"  value={this.state.npiNumber} onChange={this.npihandler} className="form-control" placeholder="Enter NPI number" />
                </div>

                <div className="form-group">
                    <label>Business address</label>
                    <input type="text"  value={this.state.businessAddress} onChange={this.bussinesshandler} className="form-control" placeholder="Enter Address" />
                </div>


                <div className="form-group">
                    <label>Telephone number</label>
                    <input type="phone"  value={this.state.telephoneNumber} onChange={this.telephonehandler} className="form-control" placeholder="Enter Telephone" />
                </div>

                <div className="form-group">
                    <label>Email address</label>
                    <input type="email"  value={this.state.emailAddress} onChange={this.emailhandler} className="form-control" placeholder="Enter email" />
                </div>

              <br/>

                <button type="submit" className="btn btn-primary btn-block">Join Availity!</button>
               
            </form>
        );
    }
}